package com.android.minimarket_app;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

/**
 * Created by Ernesto on 25/05/2018.
 */

public class ProductCarAdapter extends RecyclerView.Adapter<ProductCarAdapter.ViewHolder>{
    private ArrayList<Product> products;

    public ProductCarAdapter(ArrayList<Product> products) {
        this.products = products;
    }

    public ArrayList<Product> getProducts() {
        return products;
    }

    public void setProducts(ArrayList<Product> products) {
        this.products = products;
        this.products.addAll(products);
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_product_car, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        Glide.with(holder.pictureImageView.getContext()).load(products.get(position).getImageUrl()).into(holder.pictureImageView);
        holder.nameTextView.setText(products.get(position).getName());
        holder.priceTextView.setText(String.valueOf(products.get(position).getPrice()) + "0");
    }

    @Override
    public int getItemCount() {
        return products.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private CardView productCardView;
        private ImageView pictureImageView;
        private TextView nameTextView;
        private TextView priceTextView;

        public ViewHolder(View itemView) {
            super(itemView);
            productCardView = (CardView) itemView.findViewById(R.id.productCardView);
            pictureImageView = (ImageView) itemView.findViewById(R.id.pictureImageView);
            nameTextView = (TextView) itemView.findViewById(R.id.nameTextView);
            priceTextView = (TextView) itemView.findViewById(R.id.priceTextView);
        }
    }
}
