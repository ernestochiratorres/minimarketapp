package com.android.minimarket_app;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HomeFragment extends Fragment implements View.OnClickListener {
    private String userId;
    private static final String USER_ID = "userId";
    @BindView(R.id.productsRecyclerView)
    RecyclerView productsRecyclerView;
    @BindView(R.id.categoriesRecyclerView)
    RecyclerView categoriesRecyclerView;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.swipeRefreshLayout)
    SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.rl_view)
    RelativeLayout layout;
    @BindView(R.id.computoButton)
    LinearLayout computoButton;
    @BindView(R.id.celularesButton)
    LinearLayout celularesButton;
    @BindView(R.id.televisoresButton)
    LinearLayout televisoresButton;
    @BindView(R.id.gadgetsButton)
    LinearLayout gadgetsButton;
    @BindView(R.id.videojuegosButton)
    LinearLayout videojuegosButton;
    private ProductAdapter productAdapter;

    public HomeFragment() {
        setHasOptionsMenu(true);
    }

    public static HomeFragment newInstance(String userId) {
        HomeFragment fragment = new HomeFragment();
        Bundle args = new Bundle();
        args.putString(USER_ID, userId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

        getArgs();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);

        ButterKnife.bind(this, view);

        swipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary, R.color.colorPrimary, R.color.colorPrimary, R.color.colorPrimary);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshProducts();
            }
        });

        initViews();

        computoButton.setOnClickListener(this);
        celularesButton.setOnClickListener(this);
        televisoresButton.setOnClickListener(this);
        gadgetsButton.setOnClickListener(this);
        videojuegosButton.setOnClickListener(this);

        return view;
    }

    void getArgs() {
        if (getArguments() != null) {
            userId = getArguments().getString(USER_ID);
        }
    }

    public void initViews() {
        productsRecyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(this.getActivity(), 2);
        productsRecyclerView.setLayoutManager(layoutManager);
        showProducts("");

        RecyclerView.LayoutManager manager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        categoriesRecyclerView.setLayoutManager(manager);
    }

    public void showProducts(String category) {
        ArrayList<Product> products = new ArrayList<>();
        if (category.equals("1")) {
            products.add(new Product(1, "Chicha Morada", "este es un producto", 2.50f, "https://wongfood.vteximg.com.br/arquivos/ids/268642-750-750/415530-1.jpg?v=636830971299100000", 5));
            products.add(new Product(4, "Inca Kola 1L", "este es un producto", 4.50f, "http://www.popeyes.com.pe/media/catalog/product/cache/1/image/600x600/9df78eab33525d08d6e5fb8d27136e95/i/n/inca-kola-familiar-con-azucar_3.png.png", 5));
            products.add(new Product(5, "Coca Cola 2L", "este es un producto", 6.20f, "http://www.labrasaconcarbon.com/images/shop/coca-cola-1_25l.jpg", 5));
        } else if (category.equals("2")) {
            products.add(new Product(3, "Galleta Oreo", "este es un producto", 0.80f, "http://www.disgralec.com/1212-large_default/galletas-oreo-original-54g.jpg", 5));
        } else {
            products.add(new Product(1, "Chicha Morada", "este es un producto", 2.50f, "https://wongfood.vteximg.com.br/arquivos/ids/268642-750-750/415530-1.jpg?v=636830971299100000", 5));
            products.add(new Product(2, "Helado Donofrio", "este es un producto", 14.00f, "https://vivanda.vteximg.com.br/arquivos/ids/173737-1000-1000/20133719.jpg?v=636517341178570000", 5));
            products.add(new Product(3, "Galleta Oreo", "este es un producto", 0.80f, "http://www.disgralec.com/1212-large_default/galletas-oreo-original-54g.jpg", 5));
            products.add(new Product(4, "Inca Kola 1L", "este es un producto", 4.50f, "http://www.popeyes.com.pe/media/catalog/product/cache/1/image/600x600/9df78eab33525d08d6e5fb8d27136e95/i/n/inca-kola-familiar-con-azucar_3.png.png", 5));
            products.add(new Product(5, "Coca Cola 2L", "este es un producto", 6.20f, "http://www.labrasaconcarbon.com/images/shop/coca-cola-1_25l.jpg", 5));
            products.add(new Product(6, "Cerveza Cristal x6", "este es un producto", 21.50f, "https://entragos.com/wp-content/uploads/2018/02/cristal-latas.jpg", 5));
        }

        productAdapter = new ProductAdapter(products, getActivity());
        productsRecyclerView.setAdapter(productAdapter);
    }

    void refreshProducts() {
        showProducts("");
        onItemsLoadComplete();
    }

    void onItemsLoadComplete() {
        swipeRefreshLayout.setRefreshing(false);
    }

    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater){
        // menu.clear();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.computoButton:
                showProducts("1");
                break;
            case R.id.celularesButton:
                showProducts("2");
                break;
            case R.id.televisoresButton:
                //showProducts("3");
                break;
            case R.id.gadgetsButton:
                //showProducts("5");
                break;
            case R.id.videojuegosButton:
                //showProducts("7");
                break;
        }
    }
}
