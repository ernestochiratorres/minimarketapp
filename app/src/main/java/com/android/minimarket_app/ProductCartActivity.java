package com.android.minimarket_app;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ProductCartActivity extends BaseActivity implements View.OnClickListener {
    private String userId;
    private static final String USER_ID = "userId";
    @BindView(R.id.productsRecyclerView) RecyclerView productsRecyclerView;
    @BindView(R.id.paymentButton) Button paymentButton;
    private ProductCarAdapter productAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_cart);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        ButterKnife.bind(this);

        paymentButton.setOnClickListener(this);

        initViews();
    }

    public void initViews() {
        productsRecyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        productsRecyclerView.setLayoutManager(layoutManager);
        showProducts();
    }

    public void showProducts() {
        ArrayList<Product> products = new ArrayList<>();
        products.add(new Product(1, "Chicha Morada", "este es un producto", 2.50f, "https://wongfood.vteximg.com.br/arquivos/ids/268642-750-750/415530-1.jpg?v=636830971299100000", 5));
        products.add(new Product(3, "Galleta Oreo", "este es un producto", 0.80f, "http://www.disgralec.com/1212-large_default/galletas-oreo-original-54g.jpg", 5));

        productAdapter = new ProductCarAdapter(products);
        productsRecyclerView.setAdapter(productAdapter);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return false;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.paymentButton:
                alertDialog("Importante", "¿Desea pagar los productos que tiene en la canasta?");
                break;
        }
    }

    private void alertDialog(String title, final String message) {
        AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        alertDialog.setTitle(title);
        alertDialog.setMessage(message);
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Aceptar",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        startActivity(new Intent(getApplicationContext(), SuccessPaymentActivity.class));
                        dialog.dismiss();
                    }
                });
        alertDialog.setCancelable(false);
        alertDialog.show();
    }
}
