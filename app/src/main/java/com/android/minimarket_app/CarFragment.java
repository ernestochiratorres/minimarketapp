package com.android.minimarket_app;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CarFragment extends Fragment {
    private String userId;
    private static final String USER_ID = "userId";
    @BindView(R.id.productsRecyclerView)
    RecyclerView productsRecyclerView;
    private ProductCarAdapter productAdapter;

    public CarFragment() {
        setHasOptionsMenu(true);
    }

    public static CarFragment newInstance(String userId) {
        CarFragment fragment = new CarFragment();
        Bundle args = new Bundle();
        args.putString(USER_ID, userId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

        getArgs();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_car, container, false);

        ButterKnife.bind(this, view);

        initViews();

        return view;
    }

    void getArgs() {
        if (getArguments() != null) {
            userId = getArguments().getString(USER_ID);
        }
    }

    public void initViews() {
        productsRecyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this.getActivity());
        productsRecyclerView.setLayoutManager(layoutManager);
        showProducts();
    }

    public void showProducts() {
        ArrayList<Product> products = new ArrayList<>();
        products.add(new Product(1, "Chicha Morada", "este es un producto", 2.50f, "https://wongfood.vteximg.com.br/arquivos/ids/268642-750-750/415530-1.jpg?v=636830971299100000", 5));
        products.add(new Product(3, "Galleta Oreo", "este es un producto", 0.80f, "http://www.disgralec.com/1212-large_default/galletas-oreo-original-54g.jpg", 5));

        productAdapter = new ProductCarAdapter(products);
        productsRecyclerView.setAdapter(productAdapter);
    }

    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater){
        menu.clear();
    }
}
