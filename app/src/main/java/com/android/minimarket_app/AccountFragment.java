package com.android.minimarket_app;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AccountFragment extends Fragment {
    private String userId;
    private static final String USER_ID = "userId";
    @BindView(R.id.nameTextView) TextView nameTextView;
    @BindView(R.id.emailTextView) TextView emailTextView;

    public static AccountFragment newInstance(String userId) {
        AccountFragment fragment = new AccountFragment();
        Bundle args = new Bundle();
        args.putString(USER_ID, userId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getArgs();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_account, container, false);

        ButterKnife.bind(this, view);
        currentUser();

        return view;
    }

    void getArgs() {
        if (getArguments() != null) {
            userId = getArguments().getString(USER_ID);
        }
    }

    private void currentUser(){
        /*SharedPreferences preferences = getActivity().getSharedPreferences("preferencias_usuario", Context.MODE_PRIVATE);
        String firstName = preferences.getString("firstName", null);
        String lastName = preferences.getString("lastName", null);
        String email = preferences.getString("email", null);
        if (firstName != null){
            nameTextView.setText(firstName + " " + lastName);
            emailTextView.setText(email);
        }*/
    }
}
